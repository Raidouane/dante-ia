/*
** get_next_line.h for get_next_line.h in /home/el-mou_r/rendu/CPE/CPE_2015_getnextline
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Jan  4 11:14:39 2016 Raidouane EL MOUKHTARI
** Last update Sat May 21 05:15:18 2016 Raidouane EL MOUKHTARI
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# include <stdlib.h>
# include <unistd.h>

char	*get_next_line(const int fd);

#ifndef READ_SIZE

# define READ_SIZE (1000000)

#endif /* !READ_SIZE */

#endif /* !GET_NEXT_LINE_H_ */
