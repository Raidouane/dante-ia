/*
** astar.h for astar.h in /home/el-mou_r/rendu/IA/last_dante/dante/astar
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu May 19 03:16:54 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 04:11:03 2016 Raidouane EL MOUKHTARI
*/

#ifndef ASTAR_H_
# define ASTAR_H_

typedef struct		t_maze_
{
  int			id;
  int			size;
  int			y_max;
  char			*line;
  struct t_maze_	*next;
}			t_maze;

typedef struct		t_graph_
{
  int			id;
  int			visited;
  int			cost;
  int			x;
  int			y;
  struct t_graph_	*child[4];
  struct t_graph_	*par;
  struct t_graph_	*next;
  struct t_graph_	*prev;
}			t_graph;

typedef struct		t_sol_
{
  int			id;
  t_graph		*sol;
  struct t_sol_		*next;
  struct t_sol_		*prev;
}			t_sol;

int	create_graph(t_maze *maze, t_graph **graph, int *final);
int	my_strlen(char *s);
int	recup_map(char *s, t_maze **maze, int fd, int id);
int	padding_child(t_graph **elem);
int	check_west(char *maze, int x, t_graph **graph, t_graph *tmp);
int	fill_graph(t_graph **graph, t_graph *neigt, int dir);
int	check_east(char *maze, int x, t_graph **graph, t_graph *tmp);
int	check_south_north(char *maze, t_graph **graph, t_graph *tmp, int dir);
int	fill_mainstay(t_graph **graph, int y, int x, int *final);
int	create_mainstay(t_maze *maze, t_graph **graph, int pass, int *final);
int	check_line_neighbours(t_maze *maze, t_graph **graph, t_graph *tmp);
int	last_line(t_maze *maze, t_graph **graph, t_graph *tmp);
int	first_line(t_maze *maze, t_graph **graph, t_graph *tmp);
int	write_solution(t_maze *maze);
int	solution(t_sol **sol, t_graph *graph, int loop);
int	delete_node_sol(t_sol **sol, int x, int y);
int	find_solution(t_graph **graph, t_sol **sol);
int	best_cost(t_graph **graph);
char	*recup_maze(t_maze *maze, int y);
void	fill_last_step(t_graph *tmp, t_graph **la);
double	my_sqrt(double x);
t_maze	*recup_solution(t_maze **maze, t_sol *sol);
t_graph	*astar(t_graph **graph);

#endif /* !ASTAR_H_ */
