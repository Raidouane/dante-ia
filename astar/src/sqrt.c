/*
** sqrt.c for sqrt.c in /home/el-mou_r/rendu/IA/last_dante/dante/astar
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu May 19 01:15:35 2016 Raidouane EL MOUKHTARI
** Last update Thu May 19 18:27:46 2016 Raidouane EL MOUKHTARI
*/

#include "astar.h"

double		my_sqrt(double x)
{
  double	i;

  i = 0.000;
  while ((i * i) < x)
    i += 1;
  while ((i * i) > x)
    i -= 0.1;
  while ((i * i) < x)
    i += 0.01;
  while ((i * i) > x)
    i -= 0.001;
  return (i);
}
