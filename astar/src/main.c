/*
** main.c for main.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 11:08:40 2016 Raidouane EL MOUKHTARI
** Last update Sun May 29 21:30:25 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include "astar.h"

int	*recup_coor_final(int (*final)[2], char *s, int y, t_maze **maze)
{
  int	i;

  i = 0;
  while (s && s[i] != '\0')
    i++;
  (*final)[1] = y;
  if (s[i] == '\0' && s[i - 1] == '*')
    return ((*maze)->y_max = y, (*final)[0] = i - 1, *final);
  return (NULL);
}

int		main(int ac, char **av)
{
  t_graph	*graph;
  t_maze	*maze;
  t_sol		*sol;
  int		final[2];

  if (ac != 2)
    {
      printf("Pas ou Trop de paramètres.\n");
      return (1);
    }
  graph = NULL;
  sol = NULL;
  if (recup_map(av[1], &maze, 0, 1) == -1)
    return (1);
  if (recup_coor_final(&final, maze->line, maze->id - 1, &maze) == NULL)
    return (1);
  if (create_graph(maze, &graph, final) == -1)
    return (1);
  if ((graph = astar(&graph)) == NULL)
    return (printf("No solution found\n"), 1);
  if (find_solution(&graph, &sol) == -1)
    return (1);
  if (write_solution(recup_solution(&maze, sol)) == -1)
    return (1);
  return (0);
}
