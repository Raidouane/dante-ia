/*
** recup_map.c for recup_map.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 15:40:11 2016 Raidouane EL MOUKHTARI
** Last update Sun May 29 21:30:59 2016 Raidouane EL MOUKHTARI
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "astar.h"
#include "get_next_line.h"

int		check_line(char *s, int pass)
{
  static int	size = 0;
  int		i;

  i = 0;
  if (pass == 1)
    {
      size = my_strlen(s);
      if (s[0] != '*')
	return (-1);
    }
  else if (size != my_strlen(s))
    return (-1);
  while (s && s[i] != '\0')
    {
      if (s[i] != '*' && s[i] != 'X')
	return (-1);
      i++;
    }
  return (size);
}

int		recup_map(char *s, t_maze **maze, int fd, int id)
{
  t_maze	*elem;
  int		size;

  if ((fd = open(s, O_RDONLY)) == -1)
    return (printf("Labyrinthe inéxistant.\n"), -1);
  while ((s = get_next_line(fd)) != NULL)
  {
    if ((size = check_line(s, id)) == -1)
      return (-1);
    if ((elem = malloc(sizeof(t_maze))) == NULL)
      return (-1);
    elem->id = id;
    elem->size = size - 1;
    if ((elem->line = malloc(sizeof(char) * (size + 1))) == NULL)
      return (-1);
    elem->line = strcpy(elem->line, s);
    elem->next = *maze;
    *maze = elem;
    free(s);
    id++;
  }
  if (*maze == NULL)
    return (-1);
  return (0);
}
