/*
** algo_astar.c for algo_astar.c in /home/el-mou_r/rendu/IA/last_dante/dante/astar
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Wed May 18 03:02:25 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 04:10:10 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include "astar.h"

int	check_back(t_graph **graph)
{
  if ((*graph)->child[2] != NULL && (*graph)->child[2]->child[0] != NULL &&
      (*graph)->child[2]->child[0]->visited == 0)
    return (1);
  return (0);
}

t_graph		*best_path_next_(t_graph **graph, int cost, int *stop)
{
  if ((*graph)->child[1] != NULL && (*graph)->child[1]->cost == cost &&
      (*graph)->child[1]->visited == 0)
    {
      if (((*graph)->child[2] != NULL &&
	   (*graph)->child[2]->visited == 1 &&
	   (*graph)->child[1]->child[2] != NULL &&
	   (*graph)->child[1]->child[2]->visited == 1) ||
	  ((*graph)->child[3] != NULL &&
	   (*graph)->child[3]->visited == 1 &&
	   (*graph)->child[1]->child[3] != NULL &&
	   (*graph)->child[1]->child[3]->visited == 1))
      	return (*stop = 1, (*graph)->par);
      return ((*graph)->child[1]);
    }
  return (*stop = 1, (*graph)->par);
}

t_graph		*best_path_next(t_graph **graph, int cost, int *stop)
{
  if ((*graph)->child[2] != NULL && (*graph)->child[2]->cost == cost &&
      (*graph)->child[2]->visited == 0)
    {
      if (((*graph)->child[0] != NULL &&
	   (*graph)->child[0]->visited == 1 &&
	   (*graph)->child[2]->child[0] != NULL &&
	   (*graph)->child[2]->child[0]->visited == 1) ||
	  ((*graph)->child[1] != NULL &&
	   (*graph)->child[1]->visited == 1 &&
	   (*graph)->child[2]->child[1] != NULL &&
	   (*graph)->child[2]->child[1]->visited == 1))
      	return (*stop = 1, (*graph)->par);
      return ((*graph)->child[2]);
    }
  return (best_path_next_(graph, cost, stop));
}

t_graph		*looking_for_best_path(t_graph **graph, int cost, int *stop)
{
  if (cost == -1)
    return (*stop = 1, (*graph)->par);
  if ((*graph)->child[3] != NULL && (*graph)->child[3]->cost == cost &&
      (*graph)->child[3]->visited == 0)
    return ((*graph)->child[3]);
  if ((*graph)->child[0] != NULL && (*graph)->child[0]->cost == cost &&
      (*graph)->child[0]->visited == 0)
    {
      if (((*graph)->child[2] != NULL &&
	   (*graph)->child[2]->visited == 1 &&
	   (*graph)->child[0]->child[2] != NULL &&
	   (*graph)->child[0]->child[2]->visited == 1) ||
	  ((*graph)->child[3] != NULL &&
	   (*graph)->child[3]->visited == 1 &&
	   (*graph)->child[0]->child[3] != NULL &&
	   (*graph)->child[0]->child[3]->visited == 1));
      else
	return ((*graph)->child[0]);
    }
  return (best_path_next(graph, cost, stop));
}

t_graph		*astar(t_graph **graph)
{
  t_graph	*tmp;
  t_graph	*save;
  int		stop;

  stop = 0;
  if (*graph == NULL)
    return (NULL);
  (*graph)->visited = 1;
  if ((*graph)->cost == 0)
    return (*graph);
  tmp = *graph;
  save = looking_for_best_path(graph, best_cost(graph), &stop);
  if (stop == 1)
    return (astar(&(*graph)->par));
  graph = &save;
  (*graph)->par = tmp;
  return (astar(graph));
  return (NULL);
}
