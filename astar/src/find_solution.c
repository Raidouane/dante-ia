/*
** find_solution.c for find_solution.c in /home/el-mou_r/rendu/IA/last_dante/dante/astar
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Wed May 18 05:21:20 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 03:02:31 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include "astar.h"

int	fill_last_step_sol(t_sol *tmp, t_sol **la)
{
  tmp->next = *la;
  (*la)->prev = tmp;
  return (2);
}

int	insert_sol(t_sol **sol, t_graph *graph, int loop)
{

  static int	id = 0;
  static t_sol	*tmp = NULL;
  t_sol		*elem;

  if ((elem = malloc(sizeof(t_sol))) == NULL)
    return (-1);
  elem->id = id;
  elem->sol = graph;
  elem->next = *sol;
  if (*sol != NULL)
    (*sol)->prev = elem;
  *sol = elem;
  if (id == 0)
    tmp = *sol;
  id++;
  if (loop == 1)
    return (fill_last_step_sol(tmp, sol));
  return (0);
}

int	find_solution(t_graph **graph, t_sol **sol)
{
  t_sol	*tmp;

  if ((*graph)->x == 0 && (*graph)->y == 0)
    return (insert_sol(sol, *graph, 1));
  tmp = *sol;
  *sol = tmp;
  if (insert_sol(sol, *graph, 1) == -1)
    return (-1);
  return (find_solution(&(*graph)->par, sol));
}
