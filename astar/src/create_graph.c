/*
** create_graph.c for create_graph.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 11:12:44 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 04:09:59 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include "astar.h"

int	padding_child(t_graph **elem)
{
  int	i;

  i = 0;
  (*elem)->par = NULL;
  while (i < 4)
    {
      (*elem)->child[i] = NULL;
      i++;
    }
  return (0);
}

int	best_cost(t_graph **graph)
{
  int	cost;
  int	save;

  cost = -1;
  save = -1;
  if ((*graph)->child[3] != NULL && (*graph)->child[3]->visited == 0)
    cost = (*graph)->child[3]->cost;
  if (cost >= 0)
    save = cost;
  if ((*graph)->child[0] != NULL && (*graph)->child[0]->visited == 0)
    cost = (*graph)->child[0]->cost;
  if (cost >= 0 && (save > cost || save == -1))
    save = cost;
  if ((*graph)->child[1] != NULL && (*graph)->child[1]->visited == 0)
    cost = (*graph)->child[1]->cost;
  if (cost >= 0 && (save > cost || save == -1))
    save = cost;
  if ((*graph)->child[2] != NULL && (*graph)->child[2]->visited == 0)
    cost = (*graph)->child[2]->cost;
  if (cost >= 0 && (save > cost || save == -1))
    save = cost;
  return (save);
}

void	fill_last_step(t_graph *tmp, t_graph **la)
{
  tmp->next = *la;
  (*la)->prev = tmp;
  *la = (*la)->prev;
  return ;
}

int		create_graph(t_maze *maze, t_graph **graph, int *final)
{
  while (maze && maze->id > 1)
    {
      if (create_mainstay(maze, graph, 0, final) == -1)
	return (-1);
      maze = maze->next;
    }
  if (maze != NULL)
    if (create_mainstay(maze, graph, 1, final) == -1)
      return (-1);
  *graph = (*graph)->next;
  return (0);
}
