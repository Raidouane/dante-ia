##
## Makefile for Makefile in /home/el-mou_r/rendu/IA/dante_27_05/dante
## 
## Made by Raidouane EL MOUKHTARI
## Login   <el-mou_r@epitech.net>
## 
## Started on  Fri May 27 16:21:30 2016 Raidouane EL MOUKHTARI
## Last update Fri May 27 16:33:47 2016 Raidouane EL MOUKHTARI
##


all:
	make -C astar
	make -C largeur
	make -C profondeur
	make -C generateur

clean:	
	make clean -C astar
	make clean -C largeur
	make clean -C profondeur
	make clean -C generateur

fclean:	clean
	make fclean -C astar
	make fclean -C largeur
	make fclean -C  profondeur
	make fclean -C  generateur	

re:	fclean all
