/*
** create_graph_second.c for create_graph_second.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon May  2 01:49:44 2016 Raidouane EL MOUKHTARI
** Last update Sat May 21 05:46:02 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include "largeur.h"

int		fill_mainstay(t_graph **graph, int y, int x)
{
  t_graph	*elem;
  static int	id = 0;

  if ((elem = malloc(sizeof(t_graph))) == NULL)
    return (-1);
  elem->visited = 0;
  elem->explored = 0;
  if (id == 0)
    elem->id = -1;
  else
    elem->id = id;
  elem->x = x;
  elem->y = y;
  padding_child(&elem);
  elem->next = *graph;
  if (*graph != NULL)
    (*graph)->prev = elem;
  *graph = elem;
  id++;
  return (0);
}

int		check_south_neigh(t_maze *maze, t_graph **graph, int i, char *prev)
{
  t_graph	*tmp;
  int		stop;

  stop = 0;
  tmp = *graph;
  if (prev != NULL && prev[i] == '*')
    {
      while (tmp->next != NULL && stop == 0)
        {
          if (tmp->x == i && tmp->y == maze->id)
            {
              stop = 1;
              (*graph)->child[3] = tmp;
              tmp->child[2] = (*graph);
            }
          tmp = tmp->next;
        }
      if (tmp->x == i && tmp->y == maze->id)
        {
          (*graph)->child[3] = tmp;
          tmp = (*graph)->child[3];
          tmp->child[2] = (*graph);
        }
    }
  return (0);
}

int	check_neighbours(t_maze *maze, t_graph **graph, int i, char *prev)
{
  if (i + 1 > 0 && i + 1 <= maze->size && maze->line[i + 1] == '*')
    {
      (*graph)->child[0] = (*graph)->next;
      (*graph)->next->child[1] = *graph;
    }
  return (check_south_neigh(maze, graph, i, prev));
}

int			create_mainstay(t_maze *maze, t_graph **graph, int pass)
{
  static t_graph	*tmp = NULL;
  static char		*prev = NULL;
  int			i;

  i = maze->size;
  while (maze && i >= 0)
    {
      if (maze->line[i] == '*')
        {
          if (fill_mainstay(graph, maze->id - 1, i) == -1)
            return (-1);
          check_neighbours(maze, graph, i, prev);
          if ((*graph)->next == NULL)
            tmp = *graph;
        }
      i--;
    }
  if (pass == 1)
    fill_last_step(tmp, graph);
  prev = maze->line;
  return (0);
}

char	*recup_maze(t_maze *maze, int y)
{
  while (y < maze->id - 1)
    maze = maze->next;
  return (maze->line);
}
