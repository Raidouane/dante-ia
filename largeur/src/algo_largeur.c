/*
** algo_largeur.c for algo_largeur.c in /home/el-mou_r/rendu/IA/last_dante/dante/largeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon May 16 02:04:53 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 22:46:32 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "largeur.h"

int	fill_queue(t_queue **queue, t_graph **graph, t_graph **par, int loop)
{
  static int		id = 0;
  static t_queue	*tmp = NULL;
  t_queue		*elem;

  (*par)->visited = 1;
  (*graph)->visited = 1;
  if ((elem = malloc(sizeof(t_queue))) == NULL)
    return (-1);
  elem->id = id;
  elem->last = 1;
  elem->queue = *graph;
  elem->par = *par;
  elem->next = *queue;
  if (*queue != NULL)
    {
      (*queue)->last = 0;
      (*queue)->prev = elem;
    }
  *queue = elem;
  if (id == 0)
    tmp = *queue;
  if (loop == 1 || (*graph)->id == -1)
    return (fill_last_step_queue(tmp, queue));
  id++;
  return (0);
}

int	get_queue_next(t_graph **graph, t_queue **queue, int loop)
{
  int	val;

  if ((*graph)->child[1] != NULL && (*graph)->child[1]->visited == 0)
    {
      val = fill_queue(queue, &((*graph)->child[1]), graph, loop);
      if (val == -1 || val == 1)
	return (val);
    }
  if ((*graph)->child[2] != NULL && (*graph)->child[2]->visited == 0)
    {
      val = fill_queue(queue, &((*graph)->child[2]), graph, loop);
      if (val == -1 || val == 1)
	return (val);
    }
  return (0);
}

int	get_queue(t_graph **graph, t_queue **queue)
{
  int	loop;
  int	val;

  loop = 0;
  if ((*graph)->id == -1)
    loop = 1;
  if ((*graph)->child[3] != NULL && (*graph)->child[3]->visited == 0)
    {
      val = fill_queue(queue, &((*graph)->child[3]), graph, loop);
      if (val == -1 || val == 1)
	return (val);
    }
  if ((*graph)->child[0] != NULL && (*graph)->child[0]->visited == 0)
    {
      val = fill_queue(queue, &((*graph)->child[0]), graph, loop);
      if (val == -1 || val == 1)
	return (val);
    }
  return (get_queue_next(graph, queue, loop));
}

int	create_queue(t_queue **tmp, t_queue **queue, int *i)
{
  *i = 1;
  (*tmp)->queue->explored = 1;
  return (get_queue(&((*tmp)->queue), queue));
}

int	largeur(t_graph **graph, t_queue **queue)
{
  t_queue	*tmp;
  int		i;
  int		val;

  i = 1;
  (*graph)->explored = 1;
  if ((val = get_queue(graph, queue)) == -1 || val == 1)
    return (val);
  while (i != 0)
    {
      tmp = *queue;
      i = 0;
      if (tmp->queue->id == -1)
	return (printf("No solution found\n"), fill_queue(queue, NULL, NULL, -2));
      val = do_algo(queue, tmp, &i);
      if (val == -1 || val == 1)
	return (val);
    }
  return (0);
}
