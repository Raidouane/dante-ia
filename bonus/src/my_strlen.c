/*
** my_strlen.c for my_strlen in /home/da-fon_s/rendu/Piscine_C_J06
** 
** Made by samuel da-fonseca
** Login   <da-fon_s@epitech.net>
** 
** Started on  Tue Oct  6 11:12:09 2015 samuel da-fonseca
** Last update Thu May 19 02:53:46 2016 Raidouane EL MOUKHTARI
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str && str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}
