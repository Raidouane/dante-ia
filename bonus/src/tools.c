/*
** tools.c for tools.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon May  9 23:24:57 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 23:18:20 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include "bonus.h"

int	init_algo_prof(t_graph **graph, t_sol **sol, t_parent **par, int back[2])
{
  if (back[0] == 1)
    *par = (*par)->next;
  if ((*graph)->id == -1)
    return (solution(sol, *graph, 1));
  (*graph)->visited = 1;
  if (solution(sol, *graph, 0) == -1)
    return (-1);
  if ((*graph)->child[3] != NULL && (*graph)->child[3]->visited == 0)
    {
      if (parent(par, *graph) == -1)
        return (-1);
      return (1);
    }
  return (0);
}

int	check_possibility_north(t_graph *graph)
{
  if (graph->child[0] != NULL && graph->child[0]->visited == 0)
    return (1);
  while (graph->child[2] != NULL)
    {
      if (graph->child[2]->child[0] != NULL &&
	  graph->child[2]->child[0]->visited == 0)
	return (1);
      graph = graph->child[2];
    }
  return (0);
}

int	init_to_west(t_graph **graph, t_sol **sol, int *i, int *back[2])
{
  if (check_possibility_north(*graph) == 1)
    return (0);
  while ((*graph)->child[1] != NULL && (*graph)->child[1]->visited == 0)
    {
      if (*i == 0)
        (*graph)->visited = 0;
      else
        if (solution(sol, *graph, 0) == -1)
          return (-1);
      (*i)++;
      *graph = (*graph)->child[1];
    }
  if (*i != 0)
    {
      if (solution(sol, *graph, 0) == -1)
        return (-1);
      (*i)++;
      (*back)[0] = 0;
      (*back)[1] = *i + 1;
      return (1);
    }
  return (0);
}

int	check_east_prof(t_graph **graph, t_sol **sol, int *back[2], t_parent **par)
{
  t_sol	*tmp;
  int	i;

  i = 2;
  if ((*graph)->child[0] != NULL)
    {
      if (*par != NULL)
	if ((*graph)->x == (*par)->par->x)
	  (*back)[1] = 0;
      while ((*back)[1] > 0 && i > 0)
	{
	  tmp = (*sol)->next;;
	  free(*sol);
	  *sol = tmp;
	  i--;
	}
      if ((*back)[1] > 0)
	(*back)[1]--;
      (*back)[0] = 0;
      return (1);
    }
  return (0);
}

int	back_to_back(t_graph **graph, t_sol **sol, t_parent **par)
{
  if (*par != NULL && (*par)->next != NULL)
    {
      if (((*graph)->child[2] != NULL &&
	   (*graph)->child[2]->visited == 1)
          || (*graph)->child[2] == NULL ||
          ((*graph)->child[2] != NULL &&
	   (*graph)->child[2]->visited == 0 &&
           (*graph)->child[2]->child[1] != NULL &&
	   (*graph)->child[2]->child[1]->visited == 1))
        {
          delete_node_sol(sol, (*par)->par->x, (*par)->par->y);
	  return (1);
        }
    }
  return (0);
}
