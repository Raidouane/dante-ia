/*
** main.c for main.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 11:08:40 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 23:21:15 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include "bonus.h"

int	gateway(char *map, t_maze *maze)
{
  t_graph	*graph;
  t_sol		*sol;
  t_parent	*par;
  int		back[2];

  sol = NULL;
  graph = NULL;
  par = NULL;
  back[0] = 0;
  back[1] = 0;
  if (recup_map(map, &maze, 0, 1) == -1)
    return (-1);
  if (maze->line[maze->size] != '*')
    return (printf("ERROR: Il n'y a pas d'arrivée.\n"), -1);
  if (create_graph(maze, &graph) == -1)
    return (-1);
  back[0] = my_algo(&graph, &sol, back, &par);
  if (back[0] == -1 || back[0] != 2)
    return (printf("No solution found\n"), -1);
  if (write_solution(recup_solution(&maze, sol)) == -1)
    return (-1);
  return (0);
}

int		main(int ac, char **av)
{
  if (ac != 2)
    {
      printf("Pas ou Trop de paramètres.\n");
      return (1);
    }
  if (gateway(av[1], NULL) == -1)
    return (1);
  return (0);
}
