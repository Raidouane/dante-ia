/*
** write_into_file.c for write_into_file.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 00:36:58 2016 Raidouane EL MOUKHTARI
** Last update Thu May 19 02:10:23 2016 Raidouane EL MOUKHTARI
*/

#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "bonus.h"

int		change_maze(t_maze **maze, int y, int x)
{
  t_maze	*tmp;

  tmp = *maze;
  while ((*maze)->next != NULL && (*maze)->id != y)
    *maze = (*maze)->next;
  if ((*maze)->id == y)
    (*maze)->line[x] = ' ';
  *maze = tmp;
  return (0);
}

t_maze	*recup_solution(t_maze **maze, t_sol *sol)
{
  while (42)
    {
      change_maze(maze, sol->sol->y + 1, sol->sol->x);
      sol = sol->prev;
      if (sol->id == 0)
	return (*maze);
    }
  return (NULL);
}

int		reverse_maze(t_maze *maze, t_maze **tmp)
{
  t_maze	*elem;

  while (maze->next != NULL)
    {
      if ((elem = malloc(sizeof(t_maze))) == NULL)
	return (-1);
      elem->line = maze->line;
      elem->next = *tmp;
      *tmp = elem;
      maze = maze->next;
    }
  if (maze != NULL)
    {
      if ((elem = malloc(sizeof(t_maze))) == NULL)
	return (-1);
      elem->line = maze->line;
      elem->next = *tmp;
      *tmp = elem;
    }
  return (0);
}

int		write_solution(t_maze *maze)
{
  FILE		*fp;
  t_maze	*tmp;

  tmp = NULL;
  if ((fp = fopen("solution.txt", "w+")) == NULL)
    return (-1);
  if (chmod("solution.txt", 00777) == -1)
    return (-1);
  if (reverse_maze(maze, &tmp) == -1)
    return (-1);
  while (tmp->next != NULL)
    {
      fprintf(fp, "%s\n", tmp->line);
      printf("%s\n", tmp->line);
      tmp = tmp->next;
    }
  if (tmp != NULL)
    {
      fprintf(fp, "%s\n", tmp->line);
      printf("%s\n", tmp->line);
    }
  fclose(fp);
  return (0);
}
