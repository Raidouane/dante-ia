/*
** create_graph_second.c for create_graph_second.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon May  2 01:49:44 2016 Raidouane EL MOUKHTARI
** Last update Mon May  2 02:56:58 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include "bonus.h"

int	padding_child(t_graph **elem)
{
  int	i;

  i = 0;
  while (i < 4)
    {
      (*elem)->child[i] = NULL;
      i++;
    }
  return (0);
}

void	fill_last_step(t_graph *tmp, t_graph **la)
{
  tmp->next = *la;
  (*la)->prev = tmp;
  *la = (*la)->prev;
  return ;
}

int		fill_mainstay(t_graph **graph, int y, int x)
{
  t_graph	*elem;
  static int	id = 0;

  if ((elem = malloc(sizeof(t_graph))) == NULL)
    return (-1);
  elem->visited = 0;
  if (id == 0)
    elem->id = -1;
  else
    elem->id = id;
  elem->x = x;
  elem->y = y;
  padding_child(&elem);
  elem->next = *graph;
  if (*graph != NULL)
    (*graph)->prev = elem;
  *graph = elem;
  id++;
  return (0);
}

int			create_mainstay(t_maze *maze, t_graph **graph, int pass)
{
  static t_graph	*tmp = NULL;
  int			i;

  i = my_strlen(maze->line) - 1;
  while (maze && i >= 0)
    {
      if (maze->line[i] == '*')
        {
          if (fill_mainstay(graph, maze->id - 1, i) == -1)
            return (-1);
          if ((*graph)->next == NULL)
            tmp = *graph;
        }
      i--;
    }
  if (pass == 1)
    fill_last_step(tmp, graph);
  return (0);
}

char	*recup_maze(t_maze *maze, int y)
{
  while (y < maze->id - 1)
    maze = maze->next;
  return (maze->line);
}
