/*
** my_algo.c for my_algo.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon May  2 03:06:01 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 23:14:57 2016 Raidouane EL MOUKHTARI
*/

#include <stdio.h>
#include <stdlib.h>
#include "bonus.h"

int	fill_last_step_sol(t_sol *tmp, t_sol **la)
{
  tmp->next = *la;
  (*la)->prev = tmp;
  *la = (*la)->prev;
  return (2);
}

int		solution(t_sol **sol, t_graph *graph, int loop)
{
  static int	id = 0;
  static t_sol	*tmp = NULL;
  t_sol		*elem;

  if ((elem = malloc(sizeof(t_sol))) == NULL)
    return (-1);
  elem->id = id;
  elem->sol = graph;
  elem->next = *sol;
  if (*sol != NULL)
    (*sol)->prev = elem;
  *sol = elem;
  if (id == 0)
    tmp = *sol;
  id++;
  if (loop == 1)
    return (fill_last_step_sol(tmp, sol));
  return (0);
}

int		parent(t_parent **par, t_graph *graph)
{
  static int		id = 0;
  static t_parent	*tmp = NULL;
  t_parent		*elem;

  (void)tmp;
  if ((elem = malloc(sizeof(t_parent))) == NULL)
    return (-1);
  elem->id = id;
  elem->par = graph;
  elem->next = *par;
  if (*par != NULL)
    (*par)->prev = elem;
  *par = elem;
  if (id == 0)
    tmp = *par;
  id++;
  return (0);
}

int	delete_node_sol(t_sol **sol, int x, int y)
{
  if (*sol == NULL)
    return (0);
  while ((*sol)->sol->x != x || (*sol)->sol->y != y)
    *sol = (*sol)->next;
  if ((*sol)->sol->x == x && (*sol)->sol->y == y)
    *sol = (*sol)->next;
  return (0);
}

int	my_algo(t_graph **graph, t_sol **sol, int bk[2], t_parent **par)
{
  int	i;

  i = init_algo_prof(graph, sol, par, bk);
  if (i == -1 || i == 2)
    return (i);
  else if (i == 1)
    return (bk[0] = 0, my_algo(&((*graph)->child[3]), sol, bk, par));
  if ((i = init_to_west(graph, sol, &i, &bk)) == -1)
    return (-1);
  if (i == 1)
    return (my_algo(graph, sol, bk, par));
  if (check_east_prof(graph, sol, &bk, par) == -1)
    return (-1);
  if ((*graph)->child[0] != NULL && (*graph)->child[0]->visited == 0)
    return (my_algo(&((*graph)->child[0]), sol, bk, par));
  bk[0] = 1;
  if (back_to_back(graph, sol, par) == 1 && *par != NULL)
    return (my_algo(&((*par)->par), sol, bk, par));
  if ((*graph)->child[2] != NULL && (*graph)->child[2]->visited == 0)
    {
      if (parent(par, (*graph)->child[2]) == -1)
	return (-1);
      return (bk[0] = 0, my_algo(&((*graph)->child[2]), sol, bk, par));
    }
  return (0);
}
