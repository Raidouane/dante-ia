/*
** create_graph.c for create_graph.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 11:12:44 2016 Raidouane EL MOUKHTARI
** Last update Tue May  3 15:44:43 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include "bonus.h"

int	check_neigh(t_maze *maze, t_graph **graph, t_graph *tmp)
{
  if ((*graph)->y == 0)
    {
      if (first_line(maze, graph, tmp) == -1)
	return (-1);
    }
  else if ((*graph)->id == -1)
    {
      if (last_line(maze, graph, tmp) == -1)
	return (-1);
    }
  else
    {
      if (check_line_neighbours(maze, graph, tmp) == -1)
	return (-1);
    }
  return (0);
}

int	check_neighbours(t_maze *maze, t_graph **graph)
{
  t_graph	*tmp;

  *graph = (*graph)->next;
  tmp = *graph;
  while ((*graph)->id != -1)
    {
      check_neigh(maze, graph, tmp);
      *graph = (*graph)->next;
    }
  check_neigh(maze, graph, tmp);
  return (0);
}

int		create_graph(t_maze *maze, t_graph **graph)
{
  t_maze	*tmp;

  tmp = maze;
  while (maze && maze->id > 1)
    {
      if (create_mainstay(maze, graph, 0) == -1)
	return (-1);
      maze = maze->next;
    }
  if (maze != NULL)
    {
      if (create_mainstay(maze, graph, 1) == -1)
	return (-1);
    }
  if (check_neighbours(tmp, graph) == -1)
    return (-1);
  *graph = (*graph)->next;
  return (0);
}
