/*
** get_next_line.c for get_next_line.c in /home/el-mou_r/rendu/CPE/CPE_2015_getnextline
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Wed Jan  6 17:08:06 2016 Raidouane EL MOUKHTARI
** Last update Fri Apr 29 15:36:53 2016 Raidouane EL MOUKHTARI
*/

#include <string.h>
#include "get_next_line.h"
#include "bonus.h"

char	*gnl_else(int i, char *s, char **save, int t)
{
  char	*str;

  if (i < my_strlen(s) && (str = NULL) == NULL)
    {
      if ((*save = malloc(my_strlen(s) * sizeof(char **) * 2)) == NULL)
	return (NULL);
      i++;
      while (s && s[i] != '\0')
	{
	  (*save)[t] = s[i];
	  t++;
	  i++;
	}
      (*save)[t] = '\0';
    }
  if ((str = malloc(my_strlen(s) * 10)) == NULL)
    return (NULL);
  i = 0;
  while (s && s[i] != '\n' && s[i] != '\0')
    {
      str[i] = s[i];
      i++;
    }
  str[i] = '\0';
  return (str);
}

char	*gnl(char **save)
{
  int	i;
  char	*str;
  int	k;

  k = 0;
  str = NULL;
  i = 0;
  if ((str = malloc((my_strlen(*save) + 1) * sizeof(char *))) == NULL)
    return (NULL);
  while ((*save)[i] && (*save)[i] != '\0' && (*save)[i] != '\n')
    {
      str[i] = (*save)[i];
      i++;
    }
  str[i] = '\0';
  i++;
  (my_strlen(*save) == 1)? (k = 1) : (k = 0);
  if (i <= my_strlen(*save))
    *save = &(*save)[i];
  else
    k = 1;
  if (k == 1 || (*save)[0] == 3 || (*save)[0] == 4 ||
      (*save)[0] == '\0' || !(*save)[0])
    *save = NULL;
  return (str);
}

char	*check_if_all_is_correct(char *s, char *save, const int fd)
{
  int	len;

  len = 0;
  if ((s = malloc(READ_SIZE + 1)) == NULL)
    return (NULL);
  len = read(fd, s, READ_SIZE);
  if (len < 0)
    return (NULL);
  s[len] = '\0';
  if (s == NULL && save == NULL)
    {
      free(save);
      save = NULL;
      return (NULL);
    }
  return (s);
}

char		*check_fd_changed(char *save, int fd)
{
  static int	fdd = 0;
  static int	i = 0;

  if (i == 0)
    {
      fdd = fd;
      i++;
    }
  if (fd != fdd)
    {
      fdd = fd;
      if (save != NULL)
	save = NULL;
    }
  return (save);
}

char		*get_next_line(const int fd)
{
  static char	*save = NULL;
  int		i;
  char		*s;

  save = check_fd_changed(save, fd);
  i = 0;
  s = NULL;
  if ((s = check_if_all_is_correct(s, save, fd)) == NULL)
    return (NULL);
  if (save != NULL && my_strlen(s) == 0)
    return (gnl(&save));
  if (save && save != NULL)
    s = strcat(save, s);
  while (s && s[i] != '\0' && s[i] != '\n')
    i++;
  if (s[i] != '\n')
    {
      save = s;
      get_next_line(fd);
    }
  else if (s[i] == '\n')
    return (gnl_else(i, s, &save, 0));
  return (NULL);
}
