/*
** bonus.h for bonus.h in /home/el-mou_r/rendu/IA/last_dante/dante/bonus
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sat May 21 02:24:29 2016 Raidouane EL MOUKHTARI
** Last update Sat May 21 02:25:04 2016 Raidouane EL MOUKHTARI
*/

#ifndef BONUS_H_
# define BONUS_H_

typedef struct		t_maze_
{
  int			id;
  int			size;
  char			*line;
  struct t_maze_	*next;
}			t_maze;

typedef struct		t_graph_
{
  int			id;
  int			visited;
  int			x;
  int			y;
  struct t_graph_	*child[4];
  struct t_graph_	*next;
  struct t_graph_	*prev;
}			t_graph;

typedef struct		t_sol_
{
  int			id;
  t_graph		*sol;
  struct t_sol_		*next;
  struct t_sol_		*prev;
}			t_sol;

typedef struct		t_parent_
{
  int			id;
  t_graph		*par;
  struct t_parent_	*next;
  struct t_parent_	*prev;
}			t_parent;

int	create_graph(t_maze *maze, t_graph **graph);
int	my_strlen(char *s);
int	recup_map(char *s, t_maze **maze, int fd, int id);
int	padding_child(t_graph **elem);
int	check_west(char *maze, int x, t_graph **graph, t_graph *tmp);
int	fill_graph(t_graph **graph, t_graph *neigt, int dir);
int	check_east(char *maze, int x, t_graph **graph, t_graph *tmp);
int	check_south_north(char *maze, t_graph **graph, t_graph *tmp, int dir);
int	fill_mainstay(t_graph **graph, int y, int x);
int	create_mainstay(t_maze *maze, t_graph **graph, int pass);
int	check_line_neighbours(t_maze *maze, t_graph **graph, t_graph *tmp);
int	last_line(t_maze *maze, t_graph **graph, t_graph *tmp);
int	first_line(t_maze *maze, t_graph **graph, t_graph *tmp);
int	write_solution(t_maze *maze);
int	my_algo(t_graph **graph, t_sol **sol, int bk[2], t_parent **par);
int	init_to_west(t_graph **graph, t_sol **sol, int *i, int *back[2]);
int	init_algo_prof(t_graph **graph, t_sol **sol, t_parent **par, int back[2]);
int	solution(t_sol **sol, t_graph *graph, int loop);
int	check_east_prof(t_graph **graph, t_sol **sol, int *back[2], t_parent **par);
int	parent(t_parent **par, t_graph *graph);
int	back_to_back(t_graph **graph, t_sol **sol, t_parent **par);
int	delete_node_sol(t_sol **sol, int x, int y);
char	*recup_maze(t_maze *maze, int y);
void	fill_last_step(t_graph *tmp, t_graph **la);
t_maze	*recup_solution(t_maze **maze, t_sol *sol);

#endif /* !PROFONDEUR_H_ */
