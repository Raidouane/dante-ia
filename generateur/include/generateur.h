/*
** generateur.h for generateur.h in /home/el-mou_r/rendu/IA/last_dante/dante/generateur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu May 19 03:20:01 2016 Raidouane EL MOUKHTARI
** Last update Thu May 19 03:20:29 2016 Raidouane EL MOUKHTARI
*/

#ifndef GENERATEUR_H_
# define GENERATEUR_H_

# define SOUTH 0
# define EAST 20
# define WEST 40

typedef struct		s_maze_
{
  int			width;
  int			height;
  char			**maze;
}			s_maze;

int	error_output(int ac, char **av, s_maze *maze);
int	check_num(char *s);
int	write_maze(s_maze *maze);
int	write_maze_imp(s_maze *maze);
int	init_wall(s_maze *maze);
int	gateway(s_maze *maze);
int	gateway_imp(s_maze *maze);
int	my_strlen(char *s);
int	create_path(s_maze *maze, int x, int y);
int	add_cell_maze(s_maze *maze);
int	add_cell_maze_imp(s_maze *maze);
int	east_dig(s_maze *maze, int x, int y);
int	south_dig(s_maze *maze, int x, int y);
int	west_dig(s_maze *maze, int x, int y);
int	south_function(s_maze *maze, int *x, int *y, int *stop);
int	east_function(s_maze *maze, int *x, int *y, int *stop);
int	west_function(s_maze *maze, int *x, int *y, int *stop);
int	check_south(s_maze *maze, int x, int y);
int	check_east(s_maze *maze, int x, int y);
int	check_west(s_maze *maze, int x, int y);
int	east_dig_imp(s_maze *maze, int x, int y);
int	south_dig_imp(s_maze *maze, int x, int y);
int	west_dig_imp(s_maze *maze, int x, int y);
int	my_strstr(char *s1, char *s2, int n);
void	open_door(s_maze *maze, int x, int y);

#endif /* !GENERATEUR_H_ */
