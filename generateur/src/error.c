/*
** error.c for error.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 01:32:14 2016 Raidouane EL MOUKHTARI
** Last update Sat May 28 01:47:16 2016 Raidouane EL MOUKHTARI
*/

#include <stdio.h>
#include <stdlib.h>
#include "generateur.h"

int	error_output_next(char **av, s_maze *maze)
{
  if ((maze->width = atoi(av[1])) == 0)
    {
      printf("ERREUR: La largeur entrée est nulle.\n");
      return (1);
    }
  if ((maze->height = atoi(av[2])) == 0)
    {
      printf("ERREUR: La hauteur entrée est nulle.\n");
      return (1);
    }
  if (maze->height == 1000 && maze->width == 1000)
    return (1);
  return (0);
}

int	error_output(int ac, char **av, s_maze *maze)
{
  (void)ac;
  if (check_num(av[1]) == 1)
    {
      printf(
	     "ERREUR: La largeur entrée doit etre un nombre plus grand que 0.\n");
      return (1);
    }
  if (check_num(av[2]) == 1)
    {
      printf(
	     "ERREUR: La hauteur entrée doit etre un nombre plus grand que 0.\n");
      return (1);
    }
  return (error_output_next(av, maze));
}
