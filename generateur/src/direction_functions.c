/*
** direction_functions.c for direction_function.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 00:29:58 2016 Raidouane EL MOUKHTARI
** Last update Fri Apr 29 01:44:13 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include "generateur.h"

int	south_function(s_maze *maze, int *x, int *y, int *stop)
{
  int	val;

  val = check_south(maze, *x, *y);
  if (val == 1)
    {
      (*y)++;
      open_door(maze, *x, *y);
    }
  else if (val == 0)
    *stop = 1;
  return (0);
}

int	east_function(s_maze *maze, int *x, int *y, int *stop)
{
  int	val;

  val = check_east(maze, *x, *y);
  if (val == 1)
    {
      (*x)++;
      open_door(maze, *x, *y);
    }
  else if (val == 0)
    *stop = 1;
  return (0);
}

int	west_function(s_maze *maze, int *x, int *y, int *stop)
{
  int	val;

  val = check_west(maze, *x, *y);
  if (val == 1)
    {
      (*x)--;
      open_door(maze, *x, *y);
    }
  else if (val == 0)
    *stop = 1;
  return (0);
}

int	east_dig_imp(s_maze *maze, int x, int y)
{
  int	bool;

  bool = 0;
  while (x > 1 && x < maze->width - 1 && y > 0 && y < maze->height - 1)
    {
      if (maze->maze[y][x] == 'X')
        {
          if (rand()%3 == 1 && bool == 0)
            {
              bool = 1;
              south_dig_imp(maze, x, y);
            }
          open_door(maze, x, y);
        }
      x++;
    }
  return (0);
}

int	east_dig(s_maze *maze, int x, int y)
{
  int	bool;

  bool = 0;
  while (x > 1 && x < maze->width - 1 && y > 0 && y < maze->height - 1)
    {
      if (maze->maze[y][x] == 'X' && maze->maze[y + 1][x] == 'X'
          && maze->maze[y][x + 1] == 'X' && maze->maze[y][y - 1] == 'X')
        {
          if (rand()%3 == 1 && bool == 0)
            {
              bool = 1;
              south_dig(maze, x, y);
            }
          open_door(maze, x, y);
        }
      x++;
    }
  return (0);
}
