/*
** check_num.c for check_num.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Apr 28 01:06:49 2016 Raidouane EL MOUKHTARI
** Last update Thu Apr 28 01:08:07 2016 Raidouane EL MOUKHTARI
*/

#include "generateur.h"

int	check_num(char *s)
{
  int	i;

  i = 0;
  while (s && s[i] != '\0')
    {
      if (s[i] < '0' || s[i] > '9')
	return (1);
      i++;
    }
  return (0);
}
