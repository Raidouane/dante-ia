/*
** my_strlen.c for my_strlen.c in /home/el-mou_r/rendu/IA/last_dante/dante/generateur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu May 19 02:52:55 2016 Raidouane EL MOUKHTARI
** Last update Thu May 19 02:53:13 2016 Raidouane EL MOUKHTARI
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str && str[i] != '\0')
    i++;
  return (i);
}
