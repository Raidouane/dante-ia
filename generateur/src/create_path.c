/*
** create_path.c for create_path.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Apr 28 02:42:57 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 04:22:18 2016 Raidouane EL MOUKHTARI
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "generateur.h"

int	check_south(s_maze *maze, int x, int y)
{
  if (y == maze->height - 1)
    return (-1);
  if (y + 1 == maze->height - 1 && x == maze->width - 1)
    return (0);
  if (maze->maze[y + 1][x] == '*')
    return (-1);
  return (1);
}

int	check_east(s_maze *maze, int x, int y)
{
  if (x == maze->width - 1)
    return (-1);
  if (x + 1 == maze->width - 1 && y == maze->height - 1)
    return (0);
  if (maze->maze[y][x + 1] == '*')
    return (-1);
  return (1);
}

int	check_west(s_maze *maze, int x, int y)
{
  if (x == 0 || y == maze->height - 1)
    return (-1);
  if (maze->maze[y][x - 1] == '*')
    return (-1);
  return (1);
}

void	open_door(s_maze *maze, int x, int y)
{
  if (maze->maze[y] != NULL && maze->maze[y][x] != '\0')
    maze->maze[y][x] = '*';
}

int	create_path(s_maze *maze, int x, int y)
{
  int	direction;
  int	stop;

  srand(time(NULL));
  stop = 0;
  while (stop == 0)
    {
      direction = rand()%50;
      if (direction >= SOUTH && direction < 20)
	south_function(maze, &x, &y, &stop);
      else if (direction >= EAST && direction < 40)
	east_function(maze, &x, &y, &stop);
      if (y == maze->height - 1 && x == maze->width - 1)
	return (0);
    }
  return (0);
}
