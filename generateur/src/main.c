/*
** main.c for main.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 01:31:22 2016 Raidouane EL MOUKHTARI
** Last update Sat May 21 17:39:59 2016 Raidouane EL MOUKHTARI
*/

#include <stdio.h>
#include "generateur.h"

int		main(int ac, char **av)
{
  s_maze	maze;
  int		stop;

  stop = 0;
  if (error_output(ac, av, &maze) == 1)
    return (1);
  if (ac == 4 && my_strstr(av[3], "parfait", 7) == 1)
    if ((stop = 1) == 1 && gateway(&maze) == -1)
      return (1);
  if (ac == 3)
    {
      stop = 1;
      if (gateway_imp(&maze) == -1)
	return (1);
    }
  if (stop == 0)
    {
      printf("ERREUR: Le type passé en paramètre est incorrect ");
      printf("re-essayez: `./generate largeur hauteur [type]` avec type ");
      printf("-> parfait.\n");
      return (1);
    }
  return (0);
}
