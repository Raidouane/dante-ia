/*
** my_strstr.c for my_strstr.c in /home/el-mou_r/rendu/IA/last_dante/dante/generateur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu May 19 02:50:14 2016 Raidouane EL MOUKHTARI
** Last update Thu May 19 02:50:20 2016 Raidouane EL MOUKHTARI
*/

#include "generateur.h"

int	my_strstr(char *s1, char *s2, int n)
{
  int	i;

  i = 0;
  while (i <= n && s1[i] != '\0' && s2[i] != '\0' && s1[i] == s2[i])
    i++;
  if (i == my_strlen(s2) && my_strlen(s2) == my_strlen(s1))
    return (1);
  else
    return (0);
}
