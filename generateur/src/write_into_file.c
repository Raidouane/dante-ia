/*
** write_into_file.c for write_into_file.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 00:36:58 2016 Raidouane EL MOUKHTARI
** Last update Sun May 29 00:28:19 2016 Raidouane EL MOUKHTARI
*/

#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "generateur.h"

int	write_maze(s_maze *maze)
{
  FILE	*fp;
  int	i;

  i = 0;
  if ((fp = fopen("perfect_maze.txt", "w+")) == NULL)
    return (-1);
  if (chmod("perfect_maze.txt", 00777) == -1)
    return (-1);
  while (maze->maze && maze->maze[i] != NULL)
    {
      if (maze->maze[i + 1] != NULL)
	{
	  fprintf(fp, "%s\n", maze->maze[i]);
	  printf("%s\n", maze->maze[i]);
	}
      else if (maze->maze[i + 1] == NULL)
	{
	  fprintf(fp, "%s", maze->maze[i]);
	  printf("%s", maze->maze[i]);
	}
      i++;
    }
  fclose(fp);
  return (0);
}

int	write_maze_imp(s_maze *maze)
{
  FILE	*fp;
  int	i;

  i = 0;
  if ((fp = fopen("imperfect_maze.txt", "w+")) == NULL)
    return (-1);
  if (chmod("imperfect_maze.txt", 00777) == -1)
    return (-1);
  while (maze->maze && maze->maze[i] != NULL)
    {
      if (maze->maze[i + 1] != NULL)
	{
	  fprintf(fp, "%s\n", maze->maze[i]);
	  printf("%s\n", maze->maze[i]);
	}
      else if (maze->maze[i + 1] == NULL)
	{
	  fprintf(fp, "%s", maze->maze[i]);
	  printf("%s", maze->maze[i]);
	}
      i++;
    }
  fclose(fp);
  return (0);
}
