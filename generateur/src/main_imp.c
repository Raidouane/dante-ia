/*
** main_imp.c for main_imp.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 01:33:29 2016 Raidouane EL MOUKHTARI
** Last update Fri Apr 29 01:33:40 2016 Raidouane EL MOUKHTARI
*/

#include "generateur.h"

int		main(int ac, char **av)
{
  s_maze	maze;

  if (error_output(ac, av, &maze) == 1)
    return (1);
  if (gateway_imp(&maze) == -1)
    return (1);
  return (0);
}
