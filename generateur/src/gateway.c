/*
** gateway.c for gateway.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Apr 28 02:25:21 2016 Raidouane EL MOUKHTARI
** Last update Sat May 21 17:43:19 2016 Raidouane EL MOUKHTARI
*/

#include <stdio.h>
#include <stdlib.h>
#include "generateur.h"

int		gateway(s_maze *maze)
{
  if (init_wall(maze) == -1)
    return (-1);
  if (create_path(maze, 0, 0) == -1)
    return (-1);
  add_cell_maze(maze);
  if (write_maze(maze) == -1)
    return (-1);
  return (0);
}

int		gateway_imp(s_maze *maze)
{
  if (init_wall(maze) == -1)
    return (-1);
  if (create_path(maze, 0, 0) == -1)
    return (-1);
  add_cell_maze_imp(maze);
  if (write_maze_imp(maze) == -1)
    return (-1);
  return (0);
}
