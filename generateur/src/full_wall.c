/*
** full_wall.c for full_wall.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Apr 28 02:18:58 2016 Raidouane EL MOUKHTARI
** Last update Thu Apr 28 02:41:40 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include "generateur.h"

int	init_wall(s_maze *maze)
{
  int	i;
  int	l;

  i = 0;
  if ((maze->maze = malloc(sizeof(char *) * (maze->height + 1))) == NULL)
    return (-1);
  while (i < maze->height)
    {
      if ((maze->maze[i] = malloc(sizeof(char) * (maze->width + 1))) == NULL)
	return (-1);
      l = 0;
      while (l < maze->width)
	{
	  maze->maze[i][l] = 'X';
	  if (i == 0 && l == 0)
	    maze->maze[i][l] = '*';
	  l++;
	}
      maze->maze[i][l] = '\0';
      i++;
    }
  maze->maze[i - 1][maze->width - 1] = '*';
  maze->maze[i] = NULL;
  return (0);
}
