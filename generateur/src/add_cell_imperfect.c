/*
** add_cell.c for add_cell.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Apr 28 22:36:43 2016 Raidouane EL MOUKHTARI
** Last update Fri Apr 29 01:43:59 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "generateur.h"

int	south_dig_imp(s_maze *maze, int x, int y)
{
  int	bool;

  bool = 0;
  while (x > 1 && x < maze->width - 1 && y > 0 && y < maze->height - 1)
    {
      if (maze->maze[y][x] == 'X')
	{
	  if (rand()%5 == 1 && bool == 0)
	    {
	      bool = 1;
	      if (rand()%2 == 0)
		east_dig_imp(maze, x + 1, y);
	      else
		west_dig_imp(maze, x - 1, y);
	    }
	  open_door(maze, x, y);
	}
      y++;
    }
  return (0);
}

int	west_dig_imp(s_maze *maze, int x, int y)
{
  int	bool;

  bool = 0;
  while (x > 1 && x < maze->width - 1 && y > 0 && y < maze->height - 1)
    {
      if (maze->maze[y][x] == 'X')
	{
	  if (rand()%4 == 3 && bool == 0)
	    {
	      bool = 1;
	      south_dig_imp(maze, x, y);
	    }
	  open_door(maze, x, y);
	}
      x--;
    }
  return (0);
}

int	dig_fake_path_imp(s_maze *maze, int x, int y)
{
  int	direction;

  srand(time(NULL));
  direction = rand()%60;
  if (direction >= SOUTH && direction < 20)
    return (south_dig_imp(maze, x, y + 1));
  else if (direction >= EAST && direction < 40)
    return (east_dig_imp(maze, x + 1, y));
  else if (direction >= WEST)
    return (west_dig_imp(maze, x - 1, y));
  return (0);
}

int	dig_fake_path_on_wall_imp(s_maze *maze, int x, int y)
{
  int	direction;

  srand(time(NULL));
  direction = rand()%60;
  if (direction >= SOUTH && direction < 20)
    return (south_dig_imp(maze, x, y));
  else if (direction >= EAST && direction < 40)
    return (east_dig_imp(maze, x, y));
  else if (direction >= WEST)
    return (west_dig_imp(maze, x, y));
  return (0);
}

int	add_cell_maze_imp(s_maze *maze)
{
  int	x;
  int	y;
  int   i;

  srand(time(NULL));
  i = 0;
  while (i < maze->height * maze->width * 10)
    {
      x = rand()%(maze->width);
      y = rand()%(maze->height);
      if (maze->maze[y][x]== 'X')
	dig_fake_path_on_wall_imp(maze, x, y);
      else if (maze->maze[y][x]== '*')
	dig_fake_path_imp(maze, x, y);
      i++;
    }
  return (0);
}
