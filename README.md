# Dante

Made in 2015.

The goal of this project is to generate mazes (done in a reasonable amount of time), solve them 
(from 4 different ways and done in a reasonable amount of time), and print the solution.

#### Story:

```
"Dante, lost in a dark forest, struggles to climb a luminous hill so that he can get out. In turns, a panther,
a lion, and a wolf block the path and force him to retrace his steps. Virgil appears, who persuades him to
visit the eternal kingdoms in order to escape these perils. He offers to take him to Hell and to Purgatory
himself, and Beatrice will show him Heaven."

Theme of Canto I - The Divine Comedy - Dante Alighieri`
```

## Requirements

 - [A Unix distribution](https://www.lifewire.com/unix-flavors-list-4094248)
 - [Make](https://www.gnu.org/software/make/)
 - [GCC](https://gcc.gnu.org/)

## Compilation

In the the root of the repository, run `$> make` to build the `./generateur/generateur` (i.e: generator), 
`./profondeur/solver` (i.e: depth), `./largeur/solver` (i.e: width), `./astar/solver` executable files.

## How to use Dante ?

 - Compile the executable files (see **Compilation** above).
 
### Run the maze generator
 
 The generator `./generateur/generateur` can create two kinds of maze: [perfect](http://www.cr31.co.uk/stagecast/wang/perfect.html) and [imperfect](http://www.cr31.co.uk/stagecast/wang/imperfect.html).
 
 Usage of [graphs](http://www.cs.yale.edu/homes/aspnes/pinewiki/C(2f)Graphs.html) and [N-ary trees](https://www.geeksforgeeks.org/serialize-deserialize-n-ary-tree/to) to create this generator.
 
 The argument `x`(integer) is the maze's width and `y`(integer) the maze's height.
 
 - To generate a perfect maze:
 ```
 $> ./generateur/generateur x y parfait          
**XXXXXXXX
X**XXXXXXX
XX**XXXXXX
XXX**XXXXX
XXXX*XXXXX
XXXX*XXXXX
XXXX*XXXXX
XXXX*XXXXX
XXXX***XXX
XXXXXX****%
 ```

  - To generate an imperfect maze:
  ```
  $> ./generateur/generateur x y          
 **XXXXXXXX
 X**XXXXXXX
 XX**XXXXXX
 XXX**XXXXX
 XXXX*XXXXX
 XXXX*XXXXX
 XXXX*XXXXX
 XXXX*XXXXX
 XXXX***XXX
 XXXXXX****%
  ```

### Run the maze solvers
 
 #### The 'profendeur' solver (DFS)
  
  The 'profondeur' solver has been created using the [Depth-first search (DFS) algorithm](https://www.hackerearth.com/practice/algorithms/graphs/depth-first-search/tutorial/).
     
  
 ```
 $> ./profondeur/solver maze.txt          
ooXXXXXXXX
XooXXXXXXX
XXooXXXXXX
XXXooXXXXX
XXXXoXXXXX
XXXXoXXXXX
XXXXoXXXXX
XXXXoXXXXX
XXXXoooXXX
XXXXXXoooo%
 ```
 #### The 'largeur' solver (BFS)
   
   The 'largeur' solver has been created using the [Breadth-first search (BFS) algorithm](https://www.geeksforgeeks.org/breadth-first-search-or-bfs-for-a-graph/).
      
  
  ```
  $> ./largeur/solver maze.txt          
 ooXXXXXXXX
 XooXXXXXXX
 XXooXXXXXX
 XXXooXXXXX
 XXXXoXXXXX
 XXXXoXXXXX
 XXXXoXXXXX
 XXXXoXXXXX
 XXXXoooXXX
 XXXXXXoooo%
  ```
  
#### The 'astar' solver (A*)
   
   The 'astar' solver has been created using the [A* Search (A*) algorithm](https://www.geeksforgeeks.org/a-search-algorithm/).
      
  
  ```
  $> ./astar/solver maze.txt          
 ooXXXXXXXX
 XooXXXXXXX
 XXooXXXXXX
 XXXooXXXXX
 XXXXoXXXXX
 XXXXoXXXXX
 XXXXoXXXXX
 XXXXoXXXXX
 XXXXoooXXX
 XXXXXXoooo%
  ```
 
In self-algo directory, there is an another solver using my self algorithm.

## Author

* **Raidouane EL MOUKHTARI** ([LinkedIn](https://www.linkedin.com/in/raidouane-el-moukhtari/) / [Gitlab](https://gitlab.com/Raidouane))
