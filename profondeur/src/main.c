/*
** main.c for main.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 11:08:40 2016 Raidouane EL MOUKHTARI
** Last update Sat May 28 01:57:08 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include "profondeur.h"

int	do_last_node(t_queue **queue, t_queue *tmp, int *i)
{
  int	val;

  if (tmp != NULL && tmp->queue->explored == 0)
    {
      val = create_queue(&tmp, queue, i);
      if (val == -1 || val == 1)
	return (val);
    }
  return (0);
}

int	gateway(char *map, t_maze *maze)
{
  t_graph	*graph;
  t_queue	*queue;
  t_sol		*sol;
  int		val;

  graph = NULL;
  sol = NULL;
  queue = NULL;
  if (recup_map(map, &maze, 0, 1) == -1)
    return (-1);
  if ((maze != NULL && maze->line[maze->size] != '*' ) ||
      (maze->size + 1 == 1000 && maze->id == 1000))
    return (printf("No solution found\n"), -1);
  if (create_graph(maze, &graph) == -1)
    return (-1);
  val = profondeur(&graph, &queue, 0);
  if (val == -1)
    return (val);
  if (maze->id - 1 != queue->queue->y)
    return (printf("No solution found\n"), -1);
  if (find_sol(queue, &sol) == -1)
    return (-1);
  if (write_solution(recup_solution(&maze, sol)) == -1)
    return (-1);
  return (0);
}

int		main(int ac, char **av)
{
  t_maze	*maze;

  maze = NULL;
  if (ac != 2)
    {
      printf("Pas ou Trop de paramètres.\n");
      return (1);
    }
  if (gateway(av[1], maze) == -1)
    return (1);
  return (0);
}
