/*
** check_neighbours.c for check_neighbours.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon May  2 02:29:44 2016 Raidouane EL MOUKHTARI
** Last update Tue May  3 15:57:20 2016 Raidouane EL MOUKHTARI
*/

#include <stdio.h>
#include "profondeur.h"

int	first_line(t_maze *maze, t_graph **graph, t_graph *tmp)
{
  if ((*graph)->x < maze->size)
    if (check_east(recup_maze(maze, (*graph)->y),
		   (*graph)->x, graph, tmp) == -1)
      return (-1);
  if ((*graph)->next->id != -1)
    if (check_south_north(recup_maze(maze, (*graph)->y + 1),
			  graph, tmp, 0) == -1)
      return (-1);
  if ((*graph)->x > 0)
    if (check_west(recup_maze(maze, (*graph)->y),
		   (*graph)->x, graph, tmp) == -1)
      return (-1);
  return (0);
}

int	last_line(t_maze *maze, t_graph **graph, t_graph *tmp)
{
  if ((*graph)->y > 0)
    if (check_south_north(recup_maze(maze, (*graph)->y - 1),
			  graph, tmp, 1) == -1)
      return (-1);
  if ((*graph)->x > 0)
    if (check_west(recup_maze(maze, (*graph)->y),
		   (*graph)->x, graph, tmp) == -1)
      return (-1);
  if ((*graph)->x != maze->size)
    if (check_east(recup_maze(maze, (*graph)->y),
		   (*graph)->x, graph, tmp) == -1)
      return (-1);
  return (0);
}

int	check_line_neighbours(t_maze *maze, t_graph **graph, t_graph *tmp)
{
  if ((*graph)->y > 0)
    if (check_south_north(recup_maze(maze, (*graph)->y - 1),
			  graph, tmp, 1) == -1)
      return (-1);
  if ((*graph)->x > 0)
    if (check_west(recup_maze(maze, (*graph)->y), (*graph)->x,
		   graph, tmp) == -1)
      return (-1);
  if ((*graph)->x < maze->size)
    if (check_east(recup_maze(maze, (*graph)->y), (*graph)->x,
		   graph, tmp) == -1)
      return (-1);
  if (check_south_north(recup_maze(maze, (*graph)->y + 1),
			graph, tmp, 0) == -1)
    return (-1);
  return (0);
}
