/*
** profondeur.h for profondeur.h in /home/el-mou_r/rendu/IA/last_dante/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sat May 21 01:36:57 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 22:22:26 2016 Raidouane EL MOUKHTARI
*/

#ifndef PROFONDEUR_H_
# define PROFONDEUR_H_

typedef struct		t_maze_
{
  int			id;
  int			size;
  char			*line;
  struct t_maze_	*next;
}			t_maze;

typedef struct		t_graph_
{
  int			id;
  int			visited;
  int			explored;
  int			x;
  int			y;
  struct t_graph_	*child[4];
  struct t_graph_	*next;
  struct t_graph_	*prev;
}			t_graph;

typedef struct		t_queue_
{
  int			id;
  int			last;
  t_graph		*queue;
  t_graph		*par;
  struct t_queue_	*next;
  struct t_queue_	*prev;
}			t_queue;

typedef struct		t_sol_
{
  int			id;
  t_graph		*sol;
  struct t_sol_		*next;
  struct t_sol_		*prev;
}			t_sol;

int	create_graph(t_maze *maze, t_graph **graph);
int	my_strlen(char *s);
int	recup_map(char *s, t_maze **maze, int fd, int id);
int	padding_child(t_graph **elem);
int	check_west(char *maze, int x, t_graph **graph, t_graph *tmp);
int	fill_graph(t_graph **graph, t_graph *neigt, int dir);
int	check_east(char *maze, int x, t_graph **graph, t_graph *tmp);
int	check_south_north(char *maze, t_graph **graph, t_graph *tmp, int dir);
int	fill_mainstay(t_graph **graph, int y, int x);
int	create_mainstay(t_maze *maze, t_graph **graph, int pass);
int	check_line_neighbours(t_maze *maze, t_graph **graph, t_graph *tmp);
int	last_line(t_maze *maze, t_graph **graph, t_graph *tmp);
int	first_line(t_maze *maze, t_graph **graph, t_graph *tmp);
int	write_solution(t_maze *maze);
int	solution(t_sol **sol, t_graph *graph, int loop);
int	delete_node_sol(t_sol **sol, int x, int y);
int	profondeur(t_graph **graph, t_queue **queue, int pass);
int	find_sol(t_queue *queue, t_sol **sol);
int	do_algo(t_queue **queue, t_queue *tmp, int *i, int *back);
int	fill_last_step_queue(t_queue *tmp, t_queue **la);
int	do_last_node(t_queue **queue, t_queue *tmp, int *i);
int	create_queue(t_queue **tmp, t_queue **queue, int *i);
char	*recup_maze(t_maze *maze, int y);
void	fill_last_step(t_graph *tmp, t_graph **la);
t_maze	*recup_solution(t_maze **maze, t_sol *sol);

#endif /* !PROFONDEUR_H_ */
