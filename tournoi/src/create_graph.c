/*
** create_graph.c for create_graph.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 11:12:44 2016 Raidouane EL MOUKHTARI
** Last update Sat May 21 05:38:54 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include "tournoi.h"

int	padding_child(t_graph **elem)
{
  int	i;

  i = 0;
  while (i < 4)
    {
      (*elem)->child[i] = NULL;
      i++;
    }
  return (0);
}

void	fill_last_step(t_graph *tmp, t_graph **la)
{
  tmp->next = *la;
  (*la)->prev = tmp;
  *la = (*la)->prev;
  return ;
}

int		create_graph(t_maze *maze, t_graph **graph)
{
  while (maze && maze->id > 1)
    {
      if (create_mainstay(maze, graph, 0) == -1)
	return (-1);
      maze = maze->next;
    }
  if (maze != NULL)
    if (create_mainstay(maze, graph, 1) == -1)
      return (-1);
  *graph = (*graph)->next;
  return (0);
}
