/*
** create_graph_third.c for create_graph_third.c in /home/el-mou_r/rendu/IA/dante/profondeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon May  2 01:58:57 2016 Raidouane EL MOUKHTARI
** Last update Tue May 10 22:43:04 2016 Raidouane EL MOUKHTARI
*/

#include <stdio.h>
#include "tournoi.h"

int	fill_graph(t_graph **graph, t_graph *neigt, int dir)
{
  (*graph)->child[dir] = neigt;
  return (0);
}

int	check_west(char *maze, int x, t_graph **graph, t_graph *tmp)
{
  int	stop;

  stop = 0;
  if (x > 0 && maze[x - 1] == '*')
    {
      while (tmp->id != -1 && stop == 0)
        {
          if (tmp->x == x - 1 && tmp->y == (*graph)->y)
            {
              stop = 1;
              if (fill_graph(graph, tmp, 1) == -1)
                return (-1);
            }
          tmp = tmp->next;
        }
      if (stop == 0)
        if (tmp->x == x - 1 && tmp->y == (*graph)->y)
          if (fill_graph(graph, tmp, 1) == -1)
            return (-1);
      return (1);
    }
  return (0);
}

int	check_east(char *maze, int x, t_graph **graph, t_graph *tmp)
{
  int	stop;

  stop = 0;
  if (maze[x + 1] != '\0' && maze[x + 1] == '*')
    {
      while (tmp->id != -1 && stop == 0)
        {
          if (tmp->x == x + 1 && tmp->y == (*graph)->y)
            {
              stop = 1;
              if (fill_graph(graph, tmp, 0) == -1)
                return (-1);
            }
          tmp = tmp->next;
        }
      if (stop == 0)
        if (tmp->x == x + 1 && tmp->y == (*graph)->y)
          if (fill_graph(graph, tmp, 0) == -1)
            return (-1);
      return (1);
    }
  return (0);
}

int	match_node(t_graph **graph, t_graph *tmp, int dir, int *stop)
{
  if (dir == 0 && tmp->x == (*graph)->x && tmp->y == (*graph)->y + 1)
    {
      *stop = 1;
      if (fill_graph(graph, tmp, 3) == -1)
	return (-1);
    }
  if (dir == 1 && tmp->x == (*graph)->x && tmp->y == (*graph)->y - 1)
    {
      *stop = 1;
      if (fill_graph(graph, tmp, 2) == -1)
	return (-1);
    }
  return (0);
}

int	check_south_north(char *maze, t_graph **graph, t_graph *tmp, int dir)
{
  int	stop;

  stop = 0;
  if (maze[(*graph)->x] != '\0' && maze[(*graph)->x] == '*')
    {
      while (tmp->id != -1 && stop == 0)
        {
	  if (match_node(graph, tmp, dir, &stop) == -1)
	    return (-1);
          tmp = tmp->next;
        }
      if (stop == 0)
        {
          if (dir == 0 && tmp->x == (*graph)->x && tmp->y == (*graph)->y + 1)
	    if (fill_graph(graph, tmp, 3) == -1)
	      return (-1);
          if (dir == 1 && tmp->x == (*graph)->x && tmp->y == (*graph)->y - 1)
            if (fill_graph(graph, tmp, 2) == -1)
              return (-1);
        }
      return (1);
    }
  return (0);
}
