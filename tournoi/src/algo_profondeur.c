/*
** algo_profondeur.c for algo_profondeur.c in /home/el-mou_r/rendu/IA/last_dante/dante/BONUS
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu May 19 16:32:24 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 22:42:57 2016 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "tournoi.h"

int			fill_queue(t_queue **queue, t_graph **graph, t_graph **par, int loop)
{
  static int		id = 0;
  static t_queue	*tmp = NULL;
  t_queue		*elem;

  (*par)->visited = 1;
  (*graph)->visited = 1;
  if ((elem = malloc(sizeof(t_queue))) == NULL)
    return (-1);
  elem->id = id;
  elem->last = 1;
  elem->queue = *graph;
  elem->par = *par;
  elem->next = *queue;
  if (*queue != NULL)
    {
      (*queue)->last = 0;
      (*queue)->prev = elem;
    }
  *queue = elem;
  if (id == 0)
    tmp = *queue;
  if ((*graph)->id == -1 || loop == 1)
    return (fill_last_step_queue(tmp, queue), 1);
  id++;
  return (0);
}

int	get_queue_next(t_graph **graph, t_queue **queue, int loop)
{
  if ((*graph)->child[2] != NULL && (*graph)->child[2]->visited == 0)
    {
      if ((*graph)->child[2]->child[0] != NULL &&
	  (*graph)->child[2]->child[0]->visited == 1 &&
	  (*graph)->child[0] != NULL &&
	  (*graph)->child[0]->visited == 1)
	return (-3);
      return (fill_queue(queue, &((*graph)->child[2]), graph, loop));
    }
  if ((*graph)->child[1] != NULL && (*graph)->child[1]->visited == 0)
    {
      if ((*graph)->child[2] != NULL &&
	  (*graph)->child[2]->visited == 1 &&
	  (*graph)->child[1]->child[2] != NULL &&
	  (*graph)->child[1]->child[2]->visited == 1)
	return (-3);
      return (fill_queue(queue, &((*graph)->child[1]), graph, loop));
    }
  return (-3);
}

int	get_queue(t_graph **graph, t_queue **queue)
{
  int	loop;

  loop = 0;
  if ((*graph)->id == -1)
    loop = 1;
  if ((*graph)->child[0] != NULL && (*graph)->child[0]->visited == 0)
    return (fill_queue(queue, &((*graph)->child[0]), graph, loop));
  if ((*graph)->child[3] != NULL && (*graph)->child[3]->visited == 0)
    return (fill_queue(queue, &((*graph)->child[3]), graph, loop));
  return (get_queue_next(graph, queue, loop));
}

int	create_queue(t_queue **tmp, t_queue **queue, int *i)
{
  *i = 1;
  (*tmp)->queue->explored = 1;
  return (get_queue(&((*tmp)->queue), queue));
}

int	profondeur(t_graph **graph, t_queue **queue, int pass)
{
  t_queue	*tmp;
  int		i;
  int		val;
  int		back;

  back = 0;
  i = 1;
  (*graph)->explored = 1;
  if ((val = get_queue(graph, queue)) == -1 || val == 1)
    return (val);
  while (i != 0)
    {
      tmp = *queue;
      if ((i = 0) == 0 && tmp == NULL)
	{
	  if (pass == 2)
	    return (printf("No solution found\n"), -1);
	  return (profondeur(graph, queue, pass + 1));
	}
      if (tmp->queue->id == -1)
	return (fill_queue(queue, NULL, NULL, 1));
      if ((val = do_algo(queue, tmp, &i, &back)) == -1 || val == 1)
	return (val);
    }
  return (0);
}
