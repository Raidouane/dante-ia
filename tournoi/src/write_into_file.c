/*
** write_into_file.c for write_into_file.c in /home/el-mou_r/rendu/IA/dante/mazes
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Apr 29 00:36:58 2016 Raidouane EL MOUKHTARI
** Last update Sun May 29 00:50:00 2016 Raidouane EL MOUKHTARI
*/

#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "tournoi.h"

int		change_maze(t_maze **maze, int y, int x)
{
  t_maze	*tmp;

  tmp = *maze;
  while ((*maze)->id != y)
    *maze = (*maze)->next;
  if ((*maze)->id == y)
    (*maze)->line[x] = 'o';
  *maze = tmp;
  return (0);
}

t_maze	*recup_solution(t_maze **maze, t_sol *sol)
{
  while (42)
    {
      change_maze(maze, sol->sol->y + 1, sol->sol->x);
      sol = sol->next;
      if (sol->id == 0)
	{
	  change_maze(maze, sol->sol->y + 1, sol->sol->x);
	  return (*maze);
	}
    }
  return (NULL);
}

int		reverse_maze(t_maze *maze, t_maze **tmp)
{
  t_maze	*elem;
  int		stop;

  stop = 0;
  if (maze == NULL)
    return (-1);
  while (stop == 0)
    {
      if ((elem = malloc(sizeof(t_maze))) == NULL)
	return (-1);
      elem->line = maze->line;
      elem->next = *tmp;
      *tmp = elem;
      if (maze->id == 1)
	stop = 1;
      else
	maze = maze->next;
    }
  return (0);
}

int		write_solution(t_maze *maze)
{
  FILE		*fp;
  t_maze	*tmp;

  tmp = NULL;
  if ((fp = fopen("solution.txt", "w+")) == NULL)
    return (-1);
  if (chmod("solution.txt", 00777) == -1)
    return (-1);
  if (reverse_maze(maze, &tmp) == -1)
    return (-1);
  while (tmp->next != NULL)
    {
      fprintf(fp, "%s\n", tmp->line);
      printf("%s\n", tmp->line);
      tmp = tmp->next;
    }
  if (tmp != NULL)
    {
      fprintf(fp, "%s", tmp->line);
      printf("%s", tmp->line);
    }
  fclose(fp);
  return (0);
}
