/*
** tools.c for tools.c in /home/el-mou_r/rendu/IA/last_dante/dante/largeur
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon May 16 05:35:29 2016 Raidouane EL MOUKHTARI
** Last update Sun May 22 22:25:38 2016 Raidouane EL MOUKHTARI
*/

#include <stdio.h>
#include <stdlib.h>
#include "tournoi.h"

int	do_algo(t_queue **queue, t_queue *tmp, int *i, int *back)
{
  int	val;

  if (*back == 1)
    {
      val = create_queue(&tmp, queue, i);
      if (val == -3)
	return (tmp = tmp->next, *queue = (*queue)->next, -4);
      return (*back = 0, val);
    }
  while (tmp->next != NULL && tmp->queue->explored == 0)
    {
      if (tmp->next->queue->explored == 1 || tmp->next->next == NULL)
        {
          val = create_queue(&tmp, queue, i);
	  if (val == -3)
	    return (tmp = tmp->next, *queue = (*queue)->next, *back = 1, -4);
          else if (val == -1 || val == 1)
            return (val);
	}
      tmp = tmp->next;
    }
  return (do_last_node(queue, tmp, i));
}

int	fill_last_step_queue(t_queue *tmp, t_queue **la)
{
  tmp->next = *la;
  (*la)->prev = tmp;
  return (1);
}

int	fill_last_step_sol(t_sol *tmp, t_sol **sol)
{
  tmp->next = *sol;
  (*sol)->prev = tmp;
  return (2);
}

int	fill_sol(t_graph *queue, t_sol **sol, int loop)
{
  static int		id = 0;
  static t_sol		*tmp = NULL;
  t_sol			*elem;

  if ((elem = malloc(sizeof(t_sol))) == NULL)
    return (-1);
  elem->id = id;
  elem->sol = queue;
  elem->next = *sol;
  if (*sol != NULL)
    (*sol)->prev = elem;
  *sol = elem;
  if (id == 0)
    tmp = *sol;
  if (loop == 1)
    return (fill_last_step_sol(tmp, sol));
  id++;
  return (0);
}

int	find_sol(t_queue *queue, t_sol **sol)
{
  int	id;

  if (fill_sol(queue->queue, sol, 0) == -1)
    return (-1);
  if ((queue->queue->x == 0 && queue->queue->y == 1) ||
      (queue->queue->x == 1 && queue->queue->y == 0))
    return (fill_sol(queue->par, sol, 1));
  id = queue->par->id;
  while (queue->queue->id != id)
    queue = queue->next;
  if (queue->queue->id == id)
    return (find_sol(queue, sol));
  return (0);
}
